package com.meshgroup.task.utils.email;

/**
 * Helper utils class for validation check.
 */
public class EmailsUtils {

    public static boolean isEmailCorrect(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

}
