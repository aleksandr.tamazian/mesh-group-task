package com.meshgroup.task.utils.mapper;

import com.meshgroup.task.models.dto.FailedResponseProfileDto;
import com.meshgroup.task.models.dto.ProfileDto;
import com.meshgroup.task.models.dto.RequestProfileDto;
import com.meshgroup.task.models.Profile;
import com.meshgroup.task.models.dto.ResponseProfileDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapperUtils {

    private final ModelMapper modelMapper = new ModelMapper();

    public ProfileDto mapperToProfileDto(Profile profile) {
        return modelMapper.map(profile, ProfileDto.class);
    }

    public Profile mapperToProfile(RequestProfileDto profileDto) {
        return modelMapper.map(profileDto, Profile.class);
    }

    public ResponseProfileDto mapperToResponseProfileDto(Integer id) {
        return new ResponseProfileDto(id);
    }

    public FailedResponseProfileDto mapperToErrorResponseDto(String message) {
        return new FailedResponseProfileDto(message);
    }

    public List<ProfileDto> mapperToProfileDtoList(List<Profile> profiles) {
        List<ProfileDto> profileDtos = new ArrayList<>();

        profiles.forEach(profile -> profileDtos.add(mapperToProfileDto(profile)));

        return profileDtos;
    }
}
