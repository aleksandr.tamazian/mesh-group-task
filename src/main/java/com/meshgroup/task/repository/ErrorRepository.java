package com.meshgroup.task.repository;

import com.meshgroup.task.models.dto.ErrorResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
@Component
public final class ErrorRepository {

    // For correct usage of reading/modifying (instead of transactions).
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    // For error buffer - simple array with 1 size.
    private final ErrorResponseDto[] errorBuffer = new ErrorResponseDto[1];

    /**
     * Clear buffer.
     */
    public synchronized void flush() {
        errorBuffer[0] = null;
    }

    /**
     * Get last processed exception from buffer.
     */
    public ErrorResponseDto getLastException() {
        lock.readLock().lock();
        try {
            return errorBuffer[0];
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Process error and add to buffer.
     *
     * @param exception Exception
     */
    public void register(Exception exception) {
        log.error("Processing exception: {}", exception.getMessage());

        lock.writeLock().lock();
        try {
            errorBuffer[0] = new ErrorResponseDto(exception.getMessage(), LocalDateTime.now());
        } finally {
            lock.writeLock().unlock();
        }
    }

}
