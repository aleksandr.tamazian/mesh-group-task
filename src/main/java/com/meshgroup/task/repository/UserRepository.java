package com.meshgroup.task.repository;

import com.meshgroup.task.models.MemoryUser;
import com.meshgroup.task.models.dto.MemoryRole;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class UserRepository {

    // Instead of database usage - HashMap with login key (index).
    private static final Map<String, MemoryUser> credentials = new HashMap<>();

    // Instead of database table with roles.
    private static final MemoryRole[] roles = {new MemoryRole("ADMIN"), new MemoryRole("USER")};

    // For correct usage of reading/modifying (instead of transactions).
    private static final ReadWriteLock lock = new ReentrantReadWriteLock();

    public synchronized static void initialize() {
        if (!credentials.isEmpty()) {
            return;
        }

        // Admin user have all existing roles
        MemoryUser adminUser = new MemoryUser("Super",
                "User",
                "super-user@admin",
                "$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe",
                roles);

        credentials.put(adminUser.getLogin(), adminUser);
    }

    public static MemoryUser findByLogin(final String login) {
        try {
            lock.readLock().lock();
            return credentials.get(login);
        } finally {
            lock.readLock().unlock();
        }
    }
}
