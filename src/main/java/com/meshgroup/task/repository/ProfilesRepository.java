package com.meshgroup.task.repository;

import com.meshgroup.task.exceptions.ProfileNotFoundException;
import com.meshgroup.task.models.Profile;
import com.meshgroup.task.exceptions.AlreadyMailCreatedException;
import com.meshgroup.task.service.ErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.meshgroup.task.exceptions.ProfileExceptionMessages.*;

/**
 * Memory database presentation with modify and read operations.
 */
@Component
public final class ProfilesRepository {

    // Instead of database usage - HashMap with email key (index).
    private final Map<String, Profile> profilesIndexedByEmail = new HashMap<>();

    // Instead of database usage - HashMap with id key (index).
    private final Map<Integer, Profile> profilesIndexedById = new HashMap<>();

    // For error buffer - simple array with 1 size.
    private final Profile[] profileBuffer = new Profile[1];

    // For correct usage of reading/modifying (instead of transactions).
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    // Instead of sequence for primary IDs.
    private final AtomicInteger counter = new AtomicInteger();

    @Autowired
    private ErrorService errorService;

    /**
     * Flush all data.
     * Use synchronization for correct usage.
     */
    public synchronized void flush() {
        profilesIndexedByEmail.clear();
        errorService.flush();
        profileBuffer[0] = null;
    }

    /**
     * Creation of profile in memory.
     * Use write lock for mutual exclusion and data consistency.
     * Complexity ~ O(1) execution time.
     *
     * @param profile Profile to add
     */
    public Integer add(Profile profile) {
        lock.writeLock().lock();
        try {
            validateProfilePresenceByEmail(profile.getEmail());

            profile.setId(counter.incrementAndGet());
            profile.setCreated(LocalDateTime.now());

            profilesIndexedByEmail.put(profile.getEmail().toLowerCase(), profile);
            profilesIndexedById.put(profile.getId(), profile);
            profileBuffer[0] = profile;

            return profile.getId();
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Get last profile from buffer.
     * Use write lock for mutual exclusion and data consistency.
     * Complexity ~ O(1) execution time.
     *
     * @return Last profile from buffer
     */
    public Profile getLast() {
        lock.readLock().lock();
        try {
            return profileBuffer[0];
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get all profiles from memory.
     * Use read lock for mutual exclusion and data consistency.
     * Complexity ~ O(n) memory.
     *
     * @return List of all profiles in not idempotent order.
     */
    public List<Profile> getAll() {
        lock.readLock().lock();
        try {
            return new ArrayList<>(profilesIndexedByEmail.values());
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get profile by id.
     * Use read lock for mutual exclusion and data consistency.
     * Complexity ~ O(1) execution time.
     *
     * @param id id for search
     * @return Founded profile
     */
    public Profile getById(Integer id) {
        lock.readLock().lock();
        try {
            Profile profile = profilesIndexedById.get(id);

            if (profile == null) {
                System.out.println("test profile");
                String msg = String.format(NOT_FOUND_BY_ID.getMessage(), id);
                throw new ProfileNotFoundException(msg);
            }

            return profile;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get profile by email.
     * Use read lock for mutual exclusion and data consistency.
     * Complexity ~ O(1) execution time.
     *
     * @param email email for search
     * @return Profile
     */
    public Profile getByEmail(String email) {
        lock.readLock().lock();
        try {
            Profile profile = profilesIndexedByEmail.get(email);

            if (profile == null) {
                String msg = String.format(NOT_FOUND_BY_EMAIL.getMessage(), email);
                throw new ProfileNotFoundException(msg);
            }

            return profile;
        } finally {
            lock.readLock().unlock();
        }
    }

    private void validateProfilePresenceByEmail(String email) {
        if (profilesIndexedByEmail.containsKey(email)) {
            String msg = String.format(EMAIL_ALREADY_EXISTS.getMessage(), email);
            throw new AlreadyMailCreatedException(msg);
        }
    }
}
