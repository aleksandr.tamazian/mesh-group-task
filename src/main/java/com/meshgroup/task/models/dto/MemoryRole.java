package com.meshgroup.task.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MemoryRole {
    private final String type;
}
