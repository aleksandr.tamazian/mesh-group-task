package com.meshgroup.task.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ProfileDto {
    private Integer id;
    private String name;
    private String email;
    private Integer age;
    private LocalDateTime created;
}
