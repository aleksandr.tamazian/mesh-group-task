package com.meshgroup.task.models;

import com.meshgroup.task.models.dto.MemoryRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MemoryUser {
    private String firstName;
    private String lastName;
    private final String login;
    private String password;
    private MemoryRole[] roles;
}
