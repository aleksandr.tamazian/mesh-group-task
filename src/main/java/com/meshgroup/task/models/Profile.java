package com.meshgroup.task.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Profile {
    private Integer id;
    private String name;
    private String email;
    private Integer age;
    private LocalDateTime created;
}
