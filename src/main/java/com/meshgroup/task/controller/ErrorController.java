package com.meshgroup.task.controller;

import com.meshgroup.task.models.Profile;
import com.meshgroup.task.models.dto.ErrorResponseDto;
import com.meshgroup.task.service.ErrorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/error")
@Api(value = "/error", description = "Error controller", produces = "application/json")
public class ErrorController {

    @Autowired
    private ErrorService errorService;

    @ApiOperation(value = "Get last processed error", response = Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profile retrieved", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/last")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ErrorResponseDto last() {
        return errorService.getLast();
    }

}
