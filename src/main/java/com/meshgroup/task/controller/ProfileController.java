package com.meshgroup.task.controller;

import com.meshgroup.task.models.dto.RequestEmailProfileDto;
import com.meshgroup.task.models.dto.RequestProfileDto;
import com.meshgroup.task.models.Profile;
import com.meshgroup.task.models.dto.ProfileDto;
import com.meshgroup.task.models.dto.ResponseProfileDto;
import com.meshgroup.task.service.ProfileService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController()
@RequestMapping("/profiles")
@Api(value = "/profiles", description = "Profile controller", produces = "application/json")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @ApiOperation(value = "Create profile", response = Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profile created", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Entry not found")
    })
    @PostMapping("/set")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> create(@RequestBody @Valid RequestProfileDto profileDto) {
        ResponseProfileDto responseDto = profileService.create(profileDto);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all profiles", response = Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profiles retrieved", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<ProfileDto> getAll() {
        return profileService.getAll();
    }

    @ApiOperation(value = "Get profile by id", response = ArrayList.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profile details retrieved", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Profile not found")
    })
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> getProfileById(@PathVariable Integer id) {
        ProfileDto profileDto = profileService.getById(id);
        return new ResponseEntity<>(profileDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Get profile by email", response = Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profile retrieved", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Profile not found")
    })
    @PostMapping("/get")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> get(@RequestBody @Valid RequestEmailProfileDto profileDto) {
        ProfileDto responseDto = profileService.get(profileDto.getEmail());
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    @ApiOperation(value = "Get last added profile", response = Profile.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Profile retrieved", response = Profile.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/last")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Object> last() {
        return new ResponseEntity<>(profileService.getLast(), HttpStatus.OK);
    }
}
