package com.meshgroup.task.service;

import com.meshgroup.task.exceptions.IncorrectEmailException;
import com.meshgroup.task.exceptions.ProfileNotFoundException;
import com.meshgroup.task.models.Profile;
import com.meshgroup.task.models.dto.RequestProfileDto;
import com.meshgroup.task.models.dto.ProfileDto;
import com.meshgroup.task.models.dto.ResponseProfileDto;
import com.meshgroup.task.repository.ProfilesRepository;
import com.meshgroup.task.utils.email.EmailsUtils;
import com.meshgroup.task.utils.mapper.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.meshgroup.task.exceptions.ProfileExceptionMessages.INCORRECT_MAIL;

@Service
@Slf4j
public class ProfileService {

    @Autowired
    private MapperUtils mapperUtils;

    @Autowired
    private ProfilesRepository profilesRepository;

    /**
     * Create profile in memory.
     *
     * @param profileDto Request DTO for profile presentation
     * @return ProfileDto representation of profile
     */
    public ResponseProfileDto create(RequestProfileDto profileDto) throws IncorrectEmailException {
        log.info("Creation of profile with {}", profileDto);

        if (!EmailsUtils.isEmailCorrect(profileDto.getEmail())) {
            String msg = String.format(INCORRECT_MAIL.getMessage(), profileDto.getEmail());
            throw new IncorrectEmailException(msg);
        }

        Integer profileId = profilesRepository.add(mapperUtils.mapperToProfile(profileDto));
        return mapperUtils.mapperToResponseProfileDto(profileId);
    }

    /**
     * Retrieve all profiles from memory.
     *
     * @return List of profiles in random order.
     */
    public List<ProfileDto> getAll() {
        log.info("Retrieve all profiles");
        return mapperUtils.mapperToProfileDtoList(profilesRepository.getAll());
    }


    /**
     * Retrieve separate profile by id from memory.
     *
     * @param id Id of profile for search
     * @return ProfileDto representation
     */
    public ProfileDto getById(Integer id) throws ProfileNotFoundException {
        log.info("Retrieve profile with id={}", id);
        return mapperUtils.mapperToProfileDto(profilesRepository.getById(id));
    }

    /**
     * Retrieve profile by email from memory.
     *
     * @param email Unique email for search
     * @return ProfileDto representation
     */
    public ProfileDto get(String email) throws ProfileNotFoundException {
        log.info("Retrieve profile with email={}", email);
        Profile profile = profilesRepository.getByEmail(email.toLowerCase());
        return mapperUtils.mapperToProfileDto(profile);
    }

    /**
     * Retrieve last added profile from memory.
     *
     * @return ProfileDto representation of profile
     */
    public ProfileDto getLast() {
        log.info("Retrieve last found profile");
        Profile profile = profilesRepository.getLast();
        if (profile == null) {
            return null;
        }

        return mapperUtils.mapperToProfileDto(profile);
    }
}
