package com.meshgroup.task.service;

import com.meshgroup.task.repository.ErrorRepository;
import com.meshgroup.task.models.dto.ErrorResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ErrorService {

    @Autowired
    private ErrorRepository errorRepository;

    /**
     * Retrieve last processed error from memory buffer.
     *
     * @return Last processed Exception
     */
    public ErrorResponseDto getLast() {
        log.info("Return last processed exception");
        return errorRepository.getLastException();
    }

    /**
     * Register exception in buffer.
     *
     * @param exception Processed exception
     */
    public void registerException(Exception exception) {
        log.info("Add exception to memory");
        errorRepository.register(exception);
    }

    /**
     * Clear buffer.
     */
    public void flush() {
        errorRepository.flush();
    }
}
