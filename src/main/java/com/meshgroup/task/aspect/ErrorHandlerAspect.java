package com.meshgroup.task.aspect;

import com.meshgroup.task.exceptions.AlreadyMailCreatedException;
import com.meshgroup.task.exceptions.IncorrectEmailException;
import com.meshgroup.task.exceptions.ProfileNotFoundException;
import com.meshgroup.task.service.ErrorService;
import com.meshgroup.task.utils.mapper.MapperUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Aspect for custom application exceptions mapping.
 */
@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ErrorHandlerAspect {

    @Autowired
    private ErrorService errorService;

    @Autowired
    private MapperUtils mapperUtils;

    @ExceptionHandler(ProfileNotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(Exception exception) {
        return processFailedResponse(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {IncorrectEmailException.class, AlreadyMailCreatedException.class})
    public ResponseEntity<?> handleBadRequestException(Exception exception) {
        return processFailedResponse(exception, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<Object> processFailedResponse(Exception exception, HttpStatus status) {
        errorService.registerException(exception);
        return new ResponseEntity<>(mapperUtils.mapperToErrorResponseDto(exception.getMessage()), status);
    }
}
