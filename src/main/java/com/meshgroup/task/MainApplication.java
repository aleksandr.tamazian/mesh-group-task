package com.meshgroup.task;

import com.meshgroup.task.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainApplication {

    public static void main(String[] args) {
        // Before start Spring framework - initialize users and roles
        UserRepository.initialize();

        SpringApplication.run(MainApplication.class, args);
    }
}

