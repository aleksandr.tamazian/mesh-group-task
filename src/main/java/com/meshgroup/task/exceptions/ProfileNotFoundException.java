package com.meshgroup.task.exceptions;

public class ProfileNotFoundException extends RuntimeException {
    public ProfileNotFoundException(String msg) {
        super(msg);
    }
}
