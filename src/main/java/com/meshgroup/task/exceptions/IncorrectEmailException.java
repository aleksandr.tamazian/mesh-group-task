package com.meshgroup.task.exceptions;

public class IncorrectEmailException extends RuntimeException {
    public IncorrectEmailException(String msg) {
        super(msg);
    }
}
