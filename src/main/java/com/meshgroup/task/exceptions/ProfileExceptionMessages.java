package com.meshgroup.task.exceptions;

public enum ProfileExceptionMessages {
    NOT_FOUND_BY_EMAIL("Profile with email(%s) not found"),
    NOT_FOUND_BY_ID("Profile with id(%d) not found"),
    INCORRECT_MAIL("Email (%s) is not correct"),
    EMAIL_ALREADY_EXISTS("Profile with such email (%s) already exists in memory");

    String message;

    ProfileExceptionMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
