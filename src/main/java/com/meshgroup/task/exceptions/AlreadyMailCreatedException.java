package com.meshgroup.task.exceptions;

public class AlreadyMailCreatedException extends RuntimeException {
    public AlreadyMailCreatedException(String msg) {
        super(msg);
    }
}
