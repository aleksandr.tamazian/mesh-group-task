# Mesh Group Task

1) Задание доступно по [ссылке](https://docs.google.com/document/d/1FKiNwuKPcfVhAa0p6ipLIlyT8uTrIIo8Ii_UfgmW5GQ/edit).

### Запуск
1) `mvn clean install`
2) `mvn spring-boot:run`
3) Для получения JWT токена необходимо выполнить следующий запрос через curl (либо другую тулу):
```
curl mesh-group-task-id:XY7kmzoNzl100@localhost:8010/MeshGroupTask/oauth/token -d grant_type=password -d username=super-user@admin -d password=jwtpass
```

Пример для Postman'a:

<a href="https://ibb.co/CtJc9FT"><img src="https://i.ibb.co/cvbB8d7/image.png" alt="image" border="0"></a>

4) Для данного приложения я подключил OpenApi (swagger), поэтому пользоваться API можно через `http://localhost:8010/MeshGroupTask/swagger-ui.html`

<a href="https://ibb.co/tp1350N"><img src="https://i.ibb.co/d4H7yN3/image.png" alt="image" border="0"></a>

5) Для авторизации необходимо ввести токен формата `Bearer <token>`, где `<token>` - персональный сгенерированный токен, который имеет TTL.

<a href="https://ibb.co/qNZprf2"><img src="https://i.ibb.co/LxG1ZWs/image.png" alt="image" border="0"></a>

6) JAR лежит в папке artifact.
```
cd artifact
java -jar executable.jar
```

7) Добавлена возможность запуска приложения через docker.
```
docker build .
```

И после чего
```
docker container run -p 8010:8010 <imageId>
```
